#!/usr/bin/perl

open BOX, "<:utf8", $ARGV[0] or die($!);
open SOX, ">:utf8", $ARGV[1] or die($!);

while (<BOX>) {
	chomp;
	my ($chars, $i1, $i2, $i3, $i4, $i5) = split /\s+/g;
	if (length($chars) == 1) {
		#print STDERR "Here\n";
		print SOX "$_\n";
		next;
	}
	for (my $j = 0; $j < length($chars); $j++) {
		my $char = substr($chars, $j, 1);
		#print STDERR "There: $chars\n";
		print SOX "$char $i1 $i2 $i3 $i4 $i5\n"; 
	}
}

close BOX;
close SOX;
