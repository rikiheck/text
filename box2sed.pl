#!/usr/bin/perl

open BOX, "<:utf8", $ARGV[0];
open TXT, "<:utf8", $ARGV[1];
$pxn = 1000000;
$pxx = 0;
$pyn = 1000000;
$pyx = 0;
$pagebuf = "";
while ($line = <TXT>) {
	@words = split /\s+/, $line;
	next if $#words < 0;
	$lxn = 1000000;
	$lxx = 0;
	$lyn = 1000000;
	$lyx = 0;
	$linebuf = "";
	foreach $word (@words) {
		$xmin = 1000000;
		$xmax = 0;
		$ymin = 1000000;
		$ymax = 0;
		$w = "";
		for ($i = 0; $i < length($word); $i ++) {
			$c = substr($word, $i, 1);
			do {
				$cline = <BOX>;
			} while (substr($cline, 0, 1) ne $c);
			($xn, $yn, $xx, $yx) = substr($cline, 2) =~ /\S+/g;
			$w = $w . '\\' if $c eq '"';
			$w = $w . '\\' if $c eq '\\';
			$w = $w . substr($cline, 0, 1);
			$xmin = $xn if ($xmin > $xn);
			$xmax = $xx if ($xmax < $xx);
			$ymin = $yn if ($ymin > $yn);
			$ymax = $yx if ($ymax < $yx);
		}
		$wline = '(word ' . $xmin . ' ' . $ymin . ' ' . $xmax . ' ' . $ymax . ' "' . $w . '")';
		$linebuf = $linebuf . "\n  " . $wline;
		$lxn = $xmin if ($lxn > $xmin);
		$lxx = $xmax if ($lxx < $xmax);
		$lyn = $ymin if ($lyn > $ymin);
		$lyx = $ymax if ($lyx < $ymax);
	}
	$pagebuf = $pagebuf . "\n (line $xmin $ymin $xmax $ymax" . $linebuf . ')';
	$pxn = $lxn if ($pxn > $lxn);
	$pxx = $lxx if ($pxx < $lxx);
	$pyn = $lyn if ($pyn > $lyn);
	$pyx = $lyx if ($pyx < $lyx);
}
close BOX;
close TXT;
binmode(STDOUT, ":utf8");
print "(page $pxn $pyn $pxx $pyx", $pagebuf, ')', "\n";